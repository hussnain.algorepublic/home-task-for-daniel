package com.example.task.model;


import java.io.Serializable;

public class DataModel {
    public String name;
    public String weight;
    public int color;
    public String cost;
    public String alpaca;

    public DataModel(String name, String weight, int color, String cost, String alpaca) {
        this.name = name;
        this.weight = weight;
        this.color = color;
        this.cost = cost;
        this.alpaca = alpaca;
    }
}
