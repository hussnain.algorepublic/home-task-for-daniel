package com.example.task.ui.form;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.example.task.R;
import com.example.task.databinding.FragmentFirstBinding;
import com.example.task.helper.PreferenceUtil;
import com.example.task.model.DataModel;
import com.example.task.ui.list.ListActivity;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;

import java.util.ArrayList;

public class FormFragment extends Fragment {
    String[] farm = {"Svenssons Alpacor", "Alpacacenter", "Karlssons Farm", "Imported Alpacas"};
    FragmentFirstBinding binding;
    int selectedColor;
    boolean isColorSelected=false;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_first, container, false);
        selectedColor = getResources().getColor(R.color.black);
        setAdapter();
        binding.btColor.setOnClickListener(view -> colorPicker());

        binding.btSave.setOnClickListener(view -> {

            validateFields();

        });
        return binding.getRoot();
    }


    private void setAdapter() {
        ArrayAdapter adapter = new ArrayAdapter(getActivity(), R.layout.spinner_item_text_view, farm);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spFarm.setAdapter(adapter);

    }

    private void colorPicker() {
        ColorPickerDialogBuilder
                .with(getActivity())
                .setTitle("Choose color")
                .initialColor(selectedColor)
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .density(12)
                .setOnColorSelectedListener(new OnColorSelectedListener() {
                    @Override
                    public void onColorSelected(int sColor) {
                        binding.btColor.setBackgroundColor(sColor);
                        selectedColor = sColor;
                        isColorSelected=true;

                    }
                })
                .setPositiveButton("OK", (dialog, selectedColor, allColors) -> {
                })
                .setNegativeButton("Cancel", (dialog, which) -> {
                })
                .build()
                .show();
    }

    void validateFields(){

        if(!binding.etName.getText().toString().isEmpty()){
            if(!binding.etWeight.getText().toString().isEmpty()){
                if(isColorSelected){
                    saveData();
                }
                else{
                    Toast.makeText(getContext(),"Please select color!",Toast.LENGTH_LONG).show();
                }
            }
            else{
                Toast.makeText(getContext(),"Please enter weight!",Toast.LENGTH_LONG).show();
            }
        }
        else{
            Toast.makeText(getContext(),"Please enter alpaca name!",Toast.LENGTH_LONG).show();
        }

    }


    void saveData() {
        ArrayList<DataModel> list = new ArrayList<>();
        list.addAll(PreferenceUtil.getInstance(getActivity()).getData());
        list.add(new DataModel(binding.etName.getText().toString(), binding.etWeight.getText().toString(), selectedColor, "2", farm[binding.spFarm.getSelectedItemPosition()]));
        PreferenceUtil.getInstance(getActivity()).setData(list);
        Intent intent = new Intent(getActivity(), ListActivity.class);
        startActivity(intent);


    }

    @Override
    public void onResume() {
        super.onResume();
        isColorSelected=false;
        binding.etWeight.setText("");
        binding.etName.setText("");
        binding.btColor.setBackgroundColor(getContext().getResources().getColor(R.color.purple_200));
    }
}
