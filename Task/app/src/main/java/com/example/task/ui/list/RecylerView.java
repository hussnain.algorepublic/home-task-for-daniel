package com.example.task.ui.list;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.task.R;
import com.example.task.model.DataModel;

import java.util.ArrayList;


public class RecylerView extends RecyclerView.Adapter<RecylerView.ViewHolder> {

    private ArrayList<DataModel> data;
    private Context context;


    public RecylerView(ArrayList<DataModel> data, Context context) {
        this.data = data;
        this.context = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemLayoutView;
        itemLayoutView = LayoutInflater.from(context).inflate(R.layout.item, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder vh, final int position) {

        Log.e("Position", "" + position+ " "+ data.size());
        if (position == 0) {
            vh.llTitle.setVisibility(View.VISIBLE);
            vh.llValues.setVisibility(View.GONE);
        } else {
            int dataPos=position;
            dataPos--;
            vh.llTitle.setVisibility(View.GONE);
            vh.llValues.setVisibility(View.VISIBLE);
            vh.tvName.setText(data.get(dataPos).name);
            vh.tvWeight.setText(data.get(dataPos).weight);
            vh.vColor.setBackgroundColor(data.get(dataPos).color);
            vh.tvCost.setText(data.get(dataPos).cost);
            vh.tvAlpaca.setText(data.get(dataPos).alpaca);
        }

    }


    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName, tvWeight, tvCost, tvAlpaca;
        private View vColor;
        private LinearLayout llTitle, llValues;


        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            llTitle = itemLayoutView.findViewById(R.id.ll_title);
            llValues = itemLayoutView.findViewById(R.id.ll_values);
            tvName = itemLayoutView.findViewById(R.id.tv_name);
            tvWeight = itemLayoutView.findViewById(R.id.tv_weight);
            vColor = itemLayoutView.findViewById(R.id.v_color);
            tvCost = itemLayoutView.findViewById(R.id.tv_cost);
            tvAlpaca = itemLayoutView.findViewById(R.id.tv_alpaca);

        }
    }

    // Return the size of your data (invoked by the layout manager)
    @Override
    public int getItemCount() {


        return (data.size()+1);
    }




}