package com.example.task.ui.list;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.example.task.R;
import com.example.task.databinding.FragmentSecondBinding;
import com.example.task.helper.PreferenceUtil;
import com.example.task.model.DataModel;
import java.util.ArrayList;


public class ListActivity extends Activity {
  FragmentSecondBinding binding;
  ArrayList<DataModel> data=new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.fragment_second);
        setRecyclerView();

    }


    private void setRecyclerView() {
        data= PreferenceUtil.getInstance(this).getData();
        RecylerView adaptor = new RecylerView(data,this);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setAdapter(adaptor);
    }


}