package com.example.task.helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.task.model.DataModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;


public class PreferenceUtil  {


    private static final String PREFERENCE_NAME = "send_signal_preference";
    private static PreferenceUtil instance;
    private SharedPreferences sPref;

    private PreferenceUtil(Context context) {
        sPref = context.getSharedPreferences(PREFERENCE_NAME, MODE_PRIVATE);
    }

    public static PreferenceUtil getInstance(Context context) {
        if (instance == null) {
            instance = new PreferenceUtil(context);
        }
        return instance;
    }



    public void setData(ArrayList<DataModel> list) {
        Gson gson = new Gson();
        SharedPreferences.Editor editor = sPref.edit();
        String arrayData = gson.toJson(list);
        editor.putString("DATA_LIST", arrayData).apply();
    }


    public ArrayList<DataModel>  getData() {
        Gson gson = new Gson();
        String data=sPref.getString("DATA_LIST", "");
        if(data.isEmpty()){
            return new ArrayList<DataModel>();
        }
        return   gson.fromJson(data, new TypeToken<List<DataModel>>(){}.getType());
    }



}
